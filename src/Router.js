import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from './components/Login.vue';
import SignUp from './components/Sign-Up.vue';
import Home from './components/Home.vue';
import Showdeck from './components/ShowDeck.vue';
import PlayDeck from './components/PlayDeck.vue';
import Landing from './components/LandingPage.vue';
import firebase from 'firebase';
Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: '*',
      redirect: '/landing'
    },
    {
      path: '/',
      redirect: '/landing'
    },
    {
      path: '/landing',
      name: 'Landing',
      component: Landing
    },
    {
      name: 'login',
      path: '/login',
      component: Login
    },
    {
      path: '/sign-up',
      name: 'Login',
      component: SignUp
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/playdeck/:cardName',
      name: 'playdeck',
      component: PlayDeck,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/showdeck',
      name: 'Showdeck',
      component: Showdeck,
      meta: {
        requiresAuth: true
      }
    }
  ]
});
router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next('home');
  else next();
});

export default router;
