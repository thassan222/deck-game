import Vue from 'vue'
import App from './App.vue'
import router from './Router'
import firebase from 'firebase';
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

let app = ''

Vue.config.productionTip = false;
var firebaseConfig = {
  apiKey: "AIzaSyCKNV9sqq_e3zyOuphvihfqGh5yFrRuTKY",
  authDomain: "kidlogix-b30b5.firebaseapp.com",
  databaseURL: "https://kidlogix-b30b5.firebaseio.com",
  projectId: "kidlogix-b30b5",
  storageBucket: "",
  messagingSenderId: "175886685864",
  appId: "1:175886685864:web:1e1e8fb0369d55f8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(() =>  {
  if(!app){
    new Vue({
      router,
      render: h => h(App),
    }).$mount('#app')
  }
})

